package com.example.myclocks;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class Historial extends AppCompatActivity {

    ListView lvHistorial;
    ArrayList<String> lista;
    ArrayAdapter adaptador;

    @Override
    public void onResume() {
        super.onResume();
        setContentView(R.layout.activity_historial);

        //configuracion lista y conexion a bd
        lvHistorial = (ListView) findViewById(R.id.lvHistorial);
            GestorBD bd = new GestorBD(getApplicationContext());
            lista = bd.datos_lvCompras();
            adaptador = new ArrayAdapter(this, android.R.layout.simple_list_item_1,lista);
            lvHistorial.setAdapter(adaptador);

            //enviar nombre a detalle para hacer consulta
            lvHistorial.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> AdapterView, View view, int position, long l) {
                    Intent intent = new Intent(Historial.this, DetalleVenta.class);
                    intent.putExtra("TITULO", lista.get(position));
                    startActivity(intent);
                }
            });
        }
    }
