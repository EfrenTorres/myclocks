package com.example.myclocks;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.sql.RowId;
import java.util.ArrayList;

public class GestorBD extends SQLiteOpenHelper {
    private static final String NOMBRE_BD = "myclocksbd";
    private static final int VERSION_BD = 1;
    private static final String TABLA_PRODUCTO = "CREATE TABLE PRODUCTO(TITULO TEXT PRIMARY KEY, MARCA TEXT, PRECIO TEXT, DESCRIPCION TEXT)";
    private static final String TABLA_PROVEDOR = "CREATE TABLE PROVEDOR(RS TEXT PRIMARY KEY, ENCARGADO TEXT, TELEFONO TEXT, CORREO TEXT, HORARIO TEXT)";
    private static final String TABLA_COMPRAS = "CREATE TABLE COMPRAS(NOMBRE TEXT PRIMARY KEY, TELEFONO TEXT, DIRECCION TEXT, TARJETA TEXT, PRODUCTO TEXT, PRECIO TEXT)";

    public GestorBD (Context context){
        super(context, NOMBRE_BD, null, VERSION_BD);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(TABLA_PRODUCTO);
        sqLiteDatabase.execSQL(TABLA_PROVEDOR);
        sqLiteDatabase.execSQL(TABLA_COMPRAS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLA_PRODUCTO);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLA_PROVEDOR);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLA_COMPRAS);
        sqLiteDatabase.execSQL(TABLA_PRODUCTO);
        sqLiteDatabase.execSQL(TABLA_PROVEDOR);
        sqLiteDatabase.execSQL(TABLA_COMPRAS);
    }
    //datos predefinidos
    public void agregarPre(){
        SQLiteDatabase bd=getWritableDatabase();
        if (bd!=null){
            bd.execSQL("delete from PRODUCTO");
            bd.execSQL("delete from PROVEDOR");
            bd.execSQL("delete from COMPRAS");
            bd.execSQL("INSERT INTO PRODUCTO VALUES('OISTER PERPETUAL', 'ROLEX', '140 400','RELOJ ANTIGRAVITATORIO CON MANECILLAS DE ORO Y PLATINO.')");
            bd.execSQL("INSERT INTO PRODUCTO VALUES('SANTOS DUMONT', 'CARTIER', '200 400','Reloj modelo mediano. Caja de oro amarillo de 18 quilates, decorada con un zafiro azul. Esfera plateada. Agujas de acero azulado en forma de espada. Brazalete de oro amarillo de 18 quilates.')");
            bd.execSQL("INSERT INTO PRODUCTO VALUES('AQUARACER', 'TAG HEUER', '58 400',' Caratula negra, fechador, corona de rosca, cristal de zafiro, resistente al agua 300 metros, ajugas luminisentes, diámetro de 42mm, movimiento automstico.')");
            bd.execSQL("INSERT INTO PRODUCTO VALUES('UNWORN', 'HUBLOT', '980 927','Titanio 100%, Caucho 100%, Cristal De Zafiro 100%')");
            bd.execSQL("INSERT INTO PRODUCTO VALUES('CHRONOGRAPH', 'TISSOT', '10 400','Reloj Tissot con caja, bisel y extensible tipo brazalete en acero inoxidable color plata; carátula en tono negro, manecillas e indicadores luminiscentes, fechador y nombre de la marca a contraste.')");
            bd.execSQL("INSERT INTO PRODUCTO VALUES('SUPER SPORT', 'SEIKO', '3 650','Resistencia al Agua: 100 Metros / 10 ATM (Un reloj con resistencia al agua de 10ATM es resistente a salpicaduras y agua a presión.')");
            bd.execSQL("INSERT INTO PRODUCTO VALUES('GRANT SPORT', 'FOSSIL', '2 505','Fossil FS5268 Reloj Grant Sport, Análogo, Redondo para Hombre')");
            bd.execSQL("INSERT INTO PRODUCTO VALUES('BLACK IN JELLY', 'SWATCH', '2 300','viene con caja y correa en tono cremoso semitransparente. La peculiar estructura de la correa la hace casi invisible.')");
            bd.execSQL("INSERT INTO PROVEDOR VALUES('ROLEX MEXICO','DANIEL MTZ MTZ','5583463728','rolex@clock.com','LUNES A VIERNES 10 AM A 6 PM')");
            bd.execSQL("INSERT INTO PROVEDOR VALUES('CARTIER UNIT STATES','FRANK MYR','7054878967','cartier@clock.com','LUNES A VIERNES 09 AM A 8 PM')");
            bd.execSQL("INSERT INTO PROVEDOR VALUES('TAG HEUER ESPAÑA','GONZALO CASTELL','5612435423','tag_heuer@clock.com','LUNES A JUEVES 6 AM A 7 PM')");
            bd.execSQL("INSERT INTO PROVEDOR VALUES('HUBLOT INGLATERRA','RICK BIGRAN','7665345654','hublot@clock.com','LUNES A VIERNES 3 AM A 4 PM')");
            bd.execSQL("INSERT INTO PROVEDOR VALUES('TISSOT PORTUGAL','SARAI ESTRADA','5564562355','tissot@clock.com','LUNES Y VIERNES 11 AM A 3 PM')");
            bd.execSQL("INSERT INTO PROVEDOR VALUES('SEIKO ALEMANIA','DARA CLORKS','5739475826','seiko@clock.com','LUNES A VIERNES 3 AM A 6 PM')");
            bd.execSQL("INSERT INTO PROVEDOR VALUES('FOSSIL FRANCIA','TIN TARANTIN','9645345345','fossil@clock.com','LUNES A JUEVES 6 AM A 7 PM')");
            bd.execSQL("INSERT INTO PROVEDOR VALUES('SWATCH ITALIA','DEYNA ROBNED','3453453345','swatch@clock.com','LUNES Y MARTES 6 AM A 11 PM')");
            bd.execSQL("INSERT INTO COMPRAS VALUES('RAMON AYALA','5556678712','NUEVO LAREDO TAMAHULIPAS JACALES 115','20210301','ROLEX PLATINUM','380,000')");
            bd.execSQL("INSERT INTO COMPRAS VALUES('RIGOBERTO CARLOS','8795473815','COLONIA DOCTORES CDMX EDIFICIO 104 APARTAMENTO 23','54567623','CARTIER LUXES','20,000')");
            bd.execSQL("INSERT INTO COMPRAS VALUES('CAMILA FLORES','6849583776','CHALCO VILLA FELIX NO20 DALIAS LT12 MZ21','78594837','FOSSIL SPORT','1,200')");
            bd.close();
        }
    }

    // CRUD PARA PRODUCTOS
    public void agregarProducto(String titulo, String marca, String precio, String descripcion){
        SQLiteDatabase bd=getWritableDatabase();
        if (bd!=null){
            bd.execSQL("INSERT INTO PRODUCTO VALUES('"+titulo+"', '"+marca+"', '"+precio+"','"+descripcion+"')");
            bd.close();
        }
    }

    public void actualizarProducto(String nombre, String titulo, String marca, String precio, String descripcion){
        SQLiteDatabase bd=getWritableDatabase();
        if (bd!=null){
            bd.execSQL("UPDATE PRODUCTO SET titulo ='"+titulo+"', marca ='"+marca+"', precio ='"+precio+"', descripcion ='"+descripcion+"' WHERE titulo ='"+nombre+"'");
            bd.close();
        }
    }

    public void eliminarProducto(String id){
        SQLiteDatabase bd=getWritableDatabase();
        if (bd!=null){
            bd.execSQL("DELETE FROM PRODUCTO WHERE titulo='"+id+"'");
            bd.close();
        }
    }
    public ArrayList datos_lvProducto(){
        ArrayList<String> lista = new ArrayList<>();
        SQLiteDatabase bd = this.getReadableDatabase();
        String q = "SELECT * FROM PRODUCTO";
        Cursor registros = bd.rawQuery(q,null);
        if (registros.moveToFirst()){
            do {
                String producto = registros.getString(0);
                lista.add(producto);
            }while(registros.moveToNext());
        }
        return lista;
    }

    public  String[] buscaProducto(String buscar) {
        String[] datos = new String[4];
        SQLiteDatabase bd = this.getWritableDatabase();
        String q = "SELECT * FROM PRODUCTO WHERE TITULO ='"+buscar+"'";
        Cursor registros = bd.rawQuery(q,null);
        if (registros.moveToFirst()){
            for (int i = 0; i <= 3; i++) {
                datos[i] = registros.getString(i);
            }
        }
        return datos;
    }

    //CRUD PARA PROVEDORES
    public void agregarProvedor(String rsocial, String encargado, String telefono, String correo, String horario){
        SQLiteDatabase bd=getWritableDatabase();
        if (bd!=null){
            bd.execSQL("INSERT INTO PROVEDOR VALUES('"+rsocial+"', '"+encargado+"', '"+telefono+"','"+correo+"', '"+horario+"')");
            bd.close();
        }
    }

    public void eliminarProvedor(String id){
        SQLiteDatabase bd=getWritableDatabase();
        if (bd!=null){
            bd.execSQL("DELETE FROM PROVEDOR WHERE RS='"+id+"'");
            bd.close();
        }
    }

    public void actualizarProvedor(String nombre, String rsocial, String encargado, String telefono, String correo, String horario){
        SQLiteDatabase bd=getWritableDatabase();
        if (bd!=null){
            bd.execSQL("UPDATE PROVEDOR SET rs ='"+rsocial+"', encargado ='"+encargado+"', telefono ='"+telefono+"', correo ='"+correo+"' , horario ='"+horario+"' WHERE rs ='"+nombre+"'");
            bd.close();
        }
    }

    public ArrayList datos_lvProvedores(){
        ArrayList<String> listaP = new ArrayList<>();
        SQLiteDatabase bd = this.getReadableDatabase();
        String q = "SELECT * FROM PROVEDOR";
        Cursor registrosP = bd.rawQuery(q,null);
        if (registrosP.moveToFirst()){
            do {
                String provedor = registrosP.getString(0);
                listaP.add(provedor);
            }while(registrosP.moveToNext());
        }
        return listaP;
    }

    public  String[] buscaProvedor(String buscar) {
        String[] datos = new String[5];
        SQLiteDatabase bd = this.getWritableDatabase();
        String q = "SELECT * FROM PROVEDOR WHERE RS ='"+buscar+"'";
        Cursor registros = bd.rawQuery(q,null);
        if (registros.moveToFirst()){
            for (int i = 0; i <= 4; i++) {
                datos[i] = registros.getString(i);
            }
        }
        return datos;
    }
    // CRUD COMPRAS
    public void agregarCompra(String nombre, String telefono, String direccion, String tarjeta, String producto, String precio){
        SQLiteDatabase bd=getWritableDatabase();
        if (bd!=null){
            bd.execSQL("INSERT INTO COMPRAS VALUES('"+nombre+"', '"+telefono+"', '"+direccion+"','"+tarjeta+"', '"+producto+"', '"+precio+"')");
            bd.close();
        }
    }
    //HISTORIAL DE COMPRAS
    public ArrayList datos_lvCompras(){
        ArrayList<String> lista = new ArrayList<>();
        SQLiteDatabase bd = this.getReadableDatabase();
        String q = "SELECT * FROM COMPRAS";
        Cursor registros = bd.rawQuery(q,null);
        if (registros.moveToFirst()){
            do {
                String compra = registros.getString(0);
                lista.add(compra);
            }while(registros.moveToNext());
        }
        return lista;
    }

    public  String[] buscaVenta(String buscar) {
        String[] datos = new String[6];
        SQLiteDatabase bd = this.getWritableDatabase();
        String q = "SELECT * FROM COMPRAS WHERE nombre ='"+buscar+"'";
        Cursor registros = bd.rawQuery(q,null);
        if (registros.moveToFirst()){
            for (int i = 0; i <= 5; i++) {
                datos[i] = registros.getString(i);
            }
        }
        return datos;
    }
}
