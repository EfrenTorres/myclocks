package com.example.myclocks;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ModificarProducto extends AppCompatActivity {

    EditText edtTitulo, edtMarca, edtPrecio;
    TextView edtDesc;
    Button btnActualizar, btnEliminar;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_producto);

        //recibo nombre desde el main
        Bundle bundle = getIntent().getExtras();
        String nombre = bundle.getString("TITULO");

        //asignar los edit text
        edtTitulo=(EditText)findViewById(R.id.etxtNombre2);
        edtMarca=(EditText)findViewById(R.id.etxtMarca2);
        edtPrecio=(EditText)findViewById(R.id.etxtPrecio2);
        edtDesc=(TextView)findViewById(R.id.etxtDesc2);
        btnActualizar=(Button)findViewById(R.id.btnActProd);
        btnEliminar=(Button)findViewById(R.id.btnElimProd);

        // configuracion y conexion a bd
        try (GestorBD bd = new GestorBD(getApplicationContext())) {
            String buscar = nombre;
            String[] datos;
            datos=bd.buscaProducto(buscar);
            //mostrar la info almacenada en datos resultado de la consulta
            edtTitulo.setText(datos[0]);
            edtMarca.setText(datos[1]);
            edtPrecio.setText(datos[2]);
            edtDesc.setText(datos[3]);

            // id para titulo del producto
            id = nombre;

            // declaracion de botones
            btnActualizar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //verificar si los campos estan vacios
                    if (edtTitulo.getText().toString().isEmpty() || edtMarca.getText().toString().isEmpty() || edtPrecio.getText().toString().isEmpty() || edtDesc.getText().toString().isEmpty()) {
                        Toast.makeText(ModificarProducto.this, "ASEGURATE DE LLENAR TODOS LOS CAMPOS", Toast.LENGTH_LONG).show();
                    }else{
                        // guardar datos
                        bd.actualizarProducto(id,edtTitulo.getText().toString(), edtMarca.getText().toString(), edtPrecio.getText().toString(), edtDesc.getText().toString());
                        Toast.makeText(getApplicationContext(), "SE HA ACTUALIZADO "+id, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }

            });

            btnEliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bd.eliminarProducto(id);
                    Toast.makeText(getApplicationContext(), "SE HA ELIMINADO "+id, Toast.LENGTH_SHORT).show();
                    finish();
                }

            });
        }
    }
}