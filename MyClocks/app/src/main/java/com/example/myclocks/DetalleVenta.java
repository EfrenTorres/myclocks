package com.example.myclocks;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class DetalleVenta extends AppCompatActivity {

    TextView txtTitulo, txtPrecio, txtMarca;
    EditText edtNombre, edtNIP, edtTelefono, edtDireccion;
    Button btnLlamarCl;
    String id;
    private final int PHONE_CALL_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_venta);

        //recibo nombre desde el main
        Bundle bundle = getIntent().getExtras();
        String nombre = bundle.getString("TITULO");

        //asignar los edit text
        txtTitulo = (TextView) findViewById(R.id.txtModeloCl);
        txtPrecio = (TextView) findViewById(R.id.txtPrecioCl);
        edtNombre = (EditText) findViewById(R.id.edtNombreCl);
        edtNIP = (EditText) findViewById(R.id.edtNIPCl);
        edtTelefono = (EditText) findViewById(R.id.edtTelefonoCl);
        edtDireccion = (EditText) findViewById(R.id.edtDireccionCl);
        btnLlamarCl = (Button) findViewById(R.id.btnLlamarCliente);

        // configuracion y conexion a bd
        try (GestorBD bd = new GestorBD(getApplicationContext())) {
            String buscar = nombre;
            String[] datos;
            datos = bd.buscaVenta(buscar);
            //mostrar la info almacenada en datos resultado de la consulta
            edtNombre.setText(datos[0]);
            edtTelefono.setText(datos[1]);
            edtDireccion.setText(datos[2]);
            edtNIP.setText(datos[3]);
            txtTitulo.setText(datos[4]);
            txtPrecio.setText(datos[5]);

            // id para titulo del producto
            id = nombre;
            btnLlamarCl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String tel = edtTelefono.getText().toString();
                    if (tel != null) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PHONE_CALL_CODE);
                        } else {
                            versionAntigua(tel);
                        }
                    }
                }

                private void versiomNueva() {

                }

                private void versionAntigua(String tel) {
                    Intent intentLlamar = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + tel));
                    if (verificarPermisos(Manifest.permission.CALL_PHONE)) {
                        startActivity(intentLlamar);
                    } else {
                        Toast.makeText(DetalleVenta.this, "Active el permiso de llamadas", Toast.LENGTH_SHORT);
                    }
                }
            });
        }
    }
        @Override
        public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
            switch (requestCode) {
                case PHONE_CALL_CODE:
                    String permission = permissions[0];
                    int result = grantResults[0];
                    if (permission.equals(Manifest.permission.CALL_PHONE)) {

                        if (result == PackageManager.PERMISSION_GRANTED) {
                            String numTel = edtTelefono.getText().toString();
                            Intent llamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + numTel));
                            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
                                return;
                            startActivity(llamada);
                        } else {
                            Toast.makeText(this, "No haz autorizado el permiso", Toast.LENGTH_SHORT).show();
                        }
                    }
            }
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        private boolean verificarPermisos(String permiso){
            int resultado = this.checkCallingOrSelfPermission(permiso);
            return resultado == PackageManager.PERMISSION_GRANTED;
        }
    }