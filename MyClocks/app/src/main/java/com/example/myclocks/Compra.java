package com.example.myclocks;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Compra extends AppCompatActivity {

    TextView txtTitulo, txtMarca, txtPrecio, txtDesc;
    Button btnComprar;
    String id;
    ImageView imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compra);

        //recibo nombre desde el main
        Bundle bundle = getIntent().getExtras();
        String nombre = bundle.getString("TITULO");

        //asignar los edit text
        txtTitulo=(TextView)findViewById(R.id.txtModeloCl);
        txtMarca=(TextView)findViewById(R.id.txtMarcaStore);
        txtPrecio=(TextView)findViewById(R.id.txtPrecioCl);
        txtDesc=(TextView)findViewById(R.id.txtDescStore);
        imagen=(ImageView)findViewById(R.id.ivMarcaStore);
        btnComprar=(Button)findViewById(R.id.btnLlamarCliente);

        // configuracion y conexion a bd
        try (GestorBD bd = new GestorBD(getApplicationContext())) {
            String buscar = nombre;
            String[] datos;
            datos=bd.buscaProducto(buscar);
            //mostrar la info almacenada en datos resultado de la consulta
            txtTitulo.setText(datos[0]);
            txtMarca.setText(datos[1]);
            txtPrecio.setText(datos[2]);
            txtDesc.setText(datos[3]);

            // id para titulo del producto
            id = nombre;

            //Marcas logos
            if(txtMarca.getText().equals("ROLEX")){
                imagen.setImageResource(R.drawable.rolex);
            }else if(txtMarca.getText().equals("CARTIER")){
                imagen.setImageResource(R.drawable.cartier);
            }else if(txtMarca.getText().equals("TAG HEUER")){
                imagen.setImageResource(R.drawable.tag);
            }else if(txtMarca.getText().equals("HUBLOT")){
                imagen.setImageResource(R.drawable.hublot);
            }else if(txtMarca.getText().equals("TISSOT")){
                imagen.setImageResource(R.drawable.tissot);
            }else if(txtMarca.getText().equals("SEIKO")){
                imagen.setImageResource(R.drawable.seiko);
            }else if(txtMarca.getText().equals("SWATCH")){
                imagen.setImageResource(R.drawable.swatch);
            }else if(txtMarca.getText().equals("FOSSIL")){
                imagen.setImageResource(R.drawable.fossil);
            }

            // declaracion de botones
            btnComprar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Compra.this, Pagar.class);
                    intent.putExtra("TITULO", id);
                    startActivity(intent);
                    finish();
                }

            });

        }
    }
}