package com.example.myclocks;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Pagar extends AppCompatActivity {

    TextView txtTitulo, txtPrecio, txtMarca;
    EditText edtNombre, edtNIP, edtTelefono, edtDireccion;
    Button btnPagar;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagar);

        //recibo nombre desde el main
        Bundle bundle = getIntent().getExtras();
        String nombre = bundle.getString("TITULO");

        //asignar los edit text
        txtTitulo=(TextView)findViewById(R.id.txtModeloCl);
        txtPrecio=(TextView)findViewById(R.id.txtPrecioCl);
        edtNombre=(EditText)findViewById(R.id.edtNombreCl);
        edtNIP=(EditText)findViewById(R.id.edtNIPCl);
        edtTelefono=(EditText)findViewById(R.id.edtTelefonoCl);
        edtDireccion=(EditText)findViewById(R.id.edtDireccionCl);
        btnPagar=(Button)findViewById(R.id.btnLlamarCliente);

        // configuracion y conexion a bd
        try (GestorBD bd = new GestorBD(getApplicationContext())) {
            String buscar = nombre;
            String[] datos;
            datos=bd.buscaProducto(buscar);
            //mostrar la info almacenada en datos resultado de la consulta
            txtTitulo.setText(datos[0]);
            txtPrecio.setText(datos[2]);

            // id para titulo del producto
            id = nombre;

            // declaracion de botones
            btnPagar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //verificar si los campos estan vacios
                    if (edtNombre.getText().toString().isEmpty() || edtTelefono.getText().toString().isEmpty() || edtDireccion.getText().toString().isEmpty() || edtNIP.getText().toString().isEmpty()) {
                        Toast.makeText(Pagar.this, "ASEGURATE DE LLENAR TODOS LOS CAMPOS", Toast.LENGTH_LONG).show();
                    }else{
                        // guardar datos
                        bd.agregarCompra(edtNombre.getText().toString(), edtTelefono.getText().toString(), edtDireccion.getText().toString(), edtNIP.getText().toString(),txtTitulo.getText().toString(),txtPrecio.getText().toString());
                        bd.eliminarProducto(id);
                        Toast.makeText(getApplicationContext(), "SE REALIZO UNA COMPRA CON EXITO", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Pagar.this, Producto.class);
                        startActivity(intent);
                        finish();
                    }
                }

            });

        }
    }
}